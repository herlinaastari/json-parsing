//
//  ViewController.swift
//  JSONParsing
//
//  Created by Herlina Astari on 04/11/19.
//  Copyright © 2019 Herlina Astari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let retryButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupButton()
    }
    
    private func setupButton() {
        retryButton.backgroundColor = .black
        setupButtonConstraints()
        setupRetryButtonTarget()
    }
    
    private func setupButtonConstraints() {
        view.addSubview(retryButton)
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        
        let attributes: [NSLayoutConstraint.Attribute] = [
            .centerX,
            .centerY
        ]
        
        attributes.forEach {
            view.addConstraint(
                NSLayoutConstraint(
                    item: retryButton,
                    attribute: $0,
                    relatedBy: .equal,
                    toItem: view,
                    attribute: $0,
                    multiplier: 1,
                    constant: 0
                )
            )
        }
    }
    
    private func setupRetryButtonTarget() {
        retryButton.addTarget(self, action: #selector(fetchAPIAndParseUsingDictionary), for: .touchUpInside)
//        retryButton.addTarget(self, action: #selector(fetchAPIAndParseUsingCodable), for: .touchUpInside)
    }
    
    @objc private func fetchAPIAndParseUsingDictionary() {
        let urlString = "https://swapi.co/api/people"
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response error")
                return
            }
            
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                
                guard let jsonArray = jsonResponse as? [String: Any] else {
                    print(jsonResponse)
                    return
                }
                
                guard let results = jsonArray["results"] as? [[String: Any]] else {
                    print(jsonArray)
                    return
                }
                
                let people = results.map {
                    return People($0)
                }
                
                print(people)
            } catch let parsingError {
                print (parsingError)
            }
        }
        task.resume()
    }
    
    @objc private func fetchAPIAndParseUsingCodable() {
        let urlString = "https://swapi.co/api/people"
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                
                let model = try decoder.decode(PeopleResponse.self, from: dataResponse)
                print(model)
            } catch let parsingError {
                print (parsingError)
            }
        }
        task.resume()
    }
    
}

struct PeopleResponse: Codable {
    var results: [People]?
    
    private enum CodingKeys: String, CodingKey {
        case results
    }
}

struct People: Codable {
    var name: String?
    var skinColor: String?
    var hairColor: String?
    var eyeColor: String?
    var gender: String?
    
    init(_ dictionary: [String: Any]) {
        self.name = dictionary["name"] as? String
        self.skinColor = dictionary["skin_color"] as? String
        self.hairColor = dictionary["hair_color"] as? String
        self.eyeColor = dictionary["eye_color"] as? String
        self.gender = dictionary["gender"] as? String
    }
    
    private enum CodingKeys: String, CodingKey {
        case name, gender
        case skinColor = "skin_color"
        case hairColor = "hair_color"
        case eyeColor = "eye_color"
    }
}
